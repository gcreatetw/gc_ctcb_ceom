<?php
/*
Plugin Name: 中國信託 信用卡加密服務
Description: 中國信託 信用卡加密服務
Version: 25.02.05
Author:創意數位工作團隊
Author URI: http://www.gcreate.com.tw
Text Domain: gc-ctcb-ceom

Chandler upgrade: 22.11-17 to 22.12.13
GC_minos:2022-12-29 class-ctcb-ceom.php的disable_payment_gateway() notice修正
GC_minos:2023-01-17 修正沒有任何綁卡時卻出現付款選項的問題
GC_minos:2023-02-22 修正卡片中同時有Default=1或true的問題
GC_minos:2023-06-19 返回感謝頁後，若訂單取消改成等待付款中訂單
GC_minos:2023-09-05 新增錯誤訊息的顯示
GC_minos:2023-09-08 修正redirect參數mccargs、訂單訊息的調整
GC_minos:2023-09-20 修正redirect參數mccargs，若mccargs中有+號，從URL取下後+號會被變成空格，導致base64解出來的字串是亂碼
GC_minos:2025-02-05 關閉 PhoneNum和PhoneNumEditable，這兩個參數是之前中信要求新政策加入的參數，但實際測試會導致新增信用卡失敗

*/

define( 'GC_CTCB_DIR',  basename(dirname(__FILE__)) );

#GC_Chandler 2022-12-13
! defined( 'GC_CTCB_PATH' ) && define( 'GC_CTCB_PATH', plugin_dir_path( __FILE__ ) );
! defined( 'GC_CTCB_TEMP_PATH' ) && define( 'GC_CTCB_TEMP_PATH', GC_CTCB_PATH . 'templates/' );
# Define urls ________________________________________.
! defined( 'GC_CTCB_URL' ) && define( 'GC_CTCB_URL', plugin_dir_url( __FILE__ ) );
! defined( 'GC_CTCB_ASSETS_JS_URL' ) && define( 'GC_CTCB_ASSETS_JS_URL', GC_CTCB_URL . 'assets/js/' );
! defined( 'GC_CTCB_ASSETS_CSS_URL' ) && define( 'GC_CTCB_ASSETS_CSS_URL', GC_CTCB_URL . 'assets/css/' );

class gc_gw_ctcb_ceom
{
	private static $_instance = null;

	public static function forge()
	{
		if ( is_null( self::$_instance ) ):
			self::$_instance = new self;
		endif;

		return self::$_instance;
	}

	private function __construct()
	{
		add_action( 'plugins_loaded', array( $this, 'init_ctcb_credit' ), 100 );
	}

	public function init_ctcb_credit()
	{
		include 'classes/class-ctcb-ceom.php';
		include 'classes/class-myaccount.php';
		include 'classes/class-cron.php';
		include 'function/MacUtil.php';

		if ( ! class_exists( 'WC_Payment_Gateway' ) ):
			return;
		endif;

		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_ctcb_credit_ceom_gateway' ) );
	}

	public function add_ctcb_credit_ceom_gateway( $methods )
	{
		$methods[] = 'WC_CTCB_COEM_Payment_Gateway';

		return $methods;
	}
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	global $gc_gw_ctcb_ceom;
	$gc_gw_ctcb_ceom = gc_gw_ctcb_ceom::forge();
}