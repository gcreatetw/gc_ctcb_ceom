<?php 

/**
 * Author:GC_Kuma
 * Description:信用卡新增
 * Version:22.12.13
 * Modified By GC_Chandler
 */


defined( 'ABSPATH' ) || exit;

global $current_user;
		
$user_cards = get_user_meta( $current_user->ID, "serialize_cards", true );
$user_card_count = count( $this->filter_cards( $user_cards ) );
$disabled = $user_card_count >= 5 ? 'disabled' : '';
$disabled_message = $disabled ? '<p class="serialize_cards_hint">您已達綁卡上限，請移除已綁定的信用卡後再新增。</p>' : '';
?>
<style>
	.add_card[disabled]{
		background-color: #7d7d7d85!important;
		cursor: not-allowed;
	}
	.serialize_cards_hint{
		color:red;
	}
</style>
<div class="title">
    <h3><?php _e('綁定信用卡' , 'gc_ctcb_ceom' ); ?></h3>
	<form name="fm" action="<?php echo $url; ?>" method="POST" >
		<input type="hidden" name="reqjsonpwd" value="<?php echo $reqjsonpwd; ?>">
		<input class="btn i-block add_card" type="submit" name="submit-button" value="<?php _e('新增信用卡' , 'gc_ctcb_ceom' ); ?>" <?php echo $disabled; ?> />
		<?php echo $disabled_message; ?>
	</form>
</div>

<?php

do_action('view_edit_creditcard');

?>