<?php

/**
 * Author:GC_Chandler
 * Description:信用卡編輯頁面 front
 * Version:22.12.13
 */

defined( 'ABSPATH' ) || exit;
wp_enqueue_script('jquery.tmpl.min.js');

wp_enqueue_style('view-creditcard-list');
wp_enqueue_script('view-creditcard-list');

?>
<style>

</style>
<div class="swiper">
<div class="creditcard-row swiper-wrapper">

</div>
</div>
<p class="grey center"><?php _e('※信用卡綁定上限5張' , 'gc_ctcb_ceom'); ?></p>
<style>
    .creditcard-row .column .box {
        <?php if( wp_is_mobile() ){ ?>
        width:95%;
        height:150px;
        <?php }else{ ?>
        width: 95%;
        height:150px;
        <?php } ?>
    }
</style>