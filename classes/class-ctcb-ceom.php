<?php

class WC_CTCB_COEM_Payment_Gateway extends WC_Payment_Gateway {
		
	public function __construct(){
		
		$this->id 			= 'gc_ctcb_ceom'; //系統辨識唯一值
		$this->has_fields 	= false;
		$this->method_title = __('中國信託-信用卡', 'woocommerce'); //標題
		
		$this->init_form_fields(); //載入下列函數【init_form_fields()】的array()內容
		$this->init_settings(); //構造函數也同時應該定義和加載設定字段
		$this->title 		= $this->settings['title']; //設定的標題
		$this->description 	= $this->settings['description']; //設定的描述
		
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page') );  //需與id名稱大小寫相同
		add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page') );   			
		add_filter( 'woocommerce_available_payment_gateways', array( $this, 'disable_payment_gateway' ), 10, 1 );
		add_action( 'woocommerce_api_ceompay', [ $this, 'register_payment_method_routes' ] );
	}
	
	/****************
	 * 後台設置欄位 *
	 ****************/
	public function init_form_fields(){ 
	
		$this->form_fields = array(
		
			//是否啟用此功能的勾選框
			'enabled' => array(
				'title' => __( 'Enable/Disable', 'woocommerce' ),
				'type' => 'checkbox',
				'label' => __( '啟用 中國信託加密服務', 'woocommerce' ),
				'default' => 'yes'
			),
			//功能標題
			'title' => array(
				'title' => __('Title', 'woocommerce'),
				'type' => 'text',
				'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
				'default' => __('中國信託 - 信用卡加密服務', 'woocommerce')
			),
			//功能描述
			'description' => array(
				'title' => __('Customer Message', 'woocommerce'),
				'type' => 'textarea',
				'description' => __('', 'woocommerce'),
				'default' => __('信用卡加密服務', 'woocommerce')
			),
			'testmode' => array(
				'title'       => 'Test mode',
				'label'       => 'Enable Test Mode',
				'type'        => 'checkbox',
				'description' => 'Place the payment gateway in test mode using test API keys.',
				'default'     => 'yes',
				'desc_tip'    => true,
			),
			'gateway' => array(
				'title' => __('URL授權介面', 'woocommerce'),
				'type' => 'textarea',
				'description' => '正式環境請更改',
				'default' => 'https://testepos.ctbcbank.com/auth/SSLAuthUI.jsp',
			),
			'merchant_name' => array(
				'title' => __('商店名稱', 'woocommerce'),
				'type' => 'text',
				'description' => '',
				'default' => '台灣休閒農業發展協會'
			),				
			'mer_id' => array(
				'title' => __('特店編號', 'woocommerce'),
				'type' => 'text',
				'description' => '正式環境請更改',
				'default' => '10451'
			),
			'merchant_id' => array(
				'title' => __('MerchantID', 'woocommerce'),
				'type' => 'text',
				'description' => '正式環境請更改',
				'default' => '8220276806819'
			),	
			'terminal_id' => array(
				'title' => __('TerminalID', 'woocommerce'),
				'type' => 'text',
				'description' => '正式環境請更改',
				'default' => '90008629'
			),						
			'key' => array(
				'title' => __('壓碼', 'woocommerce'),
				'type' => 'text',
				'description' => __('壓碼請登入帳務管理系統查看', 'woocommerce'),
				'default' => 'fpKzbSjHzsKRrVLLQitJ1vLA'

			)
			
		);
		
	}	

	/****************************
	 * 結帳頁面選項的標題及描述 *
	 * 以及呼叫設定選項表單內容 *
	 ****************************/		
	public function admin_options()
	{
		?>
		<h3><?php _e('中國信託 信用卡加密服務', 'woocommerce'); ?></h3>
		<p><?php _e('中國信託 信用卡加密服務', 'woocommerce'); ?></p>
		<table class="form-table">
			<?php $this->generate_settings_html();?>
		</table><!--/.form-table-->
		<?php
		
	}
	
	public function register_payment_method_routes()
	{
		header( 'HTTP/1.1 200 OK' );
		
		update_post_meta( $_GET['order_id'], 'gc_ctcb_ceom_credit', sanitize_text_field( $_GET['ctcb_ceom_credit'] ) );
		
		$this->generate_credit_form( $_GET['order_id'] );
		
		die();
	}

	public function generate_credit_form( $order_id )
	{
		// return rest_ensure_response( 'Hello World, this is the WordPress REST API' );
		// return rest_ensure_response( 'Hello World, this is the WordPress REST API' );
	    $order 			= new WC_Order( $order_id );
		
		$credit_meta 	= maybe_unserialize( get_user_meta( $order->get_user_id(), "serialize_cards" ) );
		
		$Option 		= get_post_meta( $order_id, 'gc_ctcb_ceom_credit', true );
		//gc_log( $Option, "checkout_token" );
		$key 			= $this->settings['key'];
		$RequestNo 		= time();
		$now 			= date_i18n("Y/m/d H:i:s");
		$url 			= $this->settings['gateway'];
		$authresurl 	= plugins_url( GC_CTCB_DIR ) . '/classes/class-ctcb-ceom-callback.php';
		//$authresurl = plugins_url( 'wc-gw-ctcb-credit' ) . '/result.php'; //要放callback解密檔案
		
		$args = [
			'MerID' => $this->settings['mer_id'],
			'MemberID' => $order->get_user_id(),
			'TerminalID' => $this->settings['terminal_id'],
			'Lidm' => $order_id,
			'PurchAmt' => round($order->get_total()),
			'TxType' => 0,
			'AuthResURL' => $authresurl,
			'AutoCap' => 1,
			'RequestNo' => $RequestNo,
			'Token' => $Option,
		];
		
		arsort($args);
		gc_log($args, 'gc_ctcb_ceom_args');

		$data = '';
		foreach($args as $k => $value){
			$data .= "&$k=$value";
		}
		$data = substr($data, 1);
		
		$fastpay = new MacUtil;
		$txn = $fastpay->getTXN($data,$key);

		$mac = $fastpay->getMAC($data,$key);
			
		$secpMap = array(
			"HexMac" => $mac,
			"SecData" => $txn
		);
		$Req = array(
			"Request" => array(
				"Header" => array(
					"ServiceName" 	=> "Pay",
					"Version" 		=> "1.0",
					"MerchantID" 	=> $this->settings['merchant_id'],
					"RequestTime" 	=> $now
				),
				"Data" => array(
					"MAC" => $mac,
					"TXN" => $txn
				)
			)
		);
		$reqjsonpwd = json_encode($Req,JSON_UNESCAPED_SLASHES);

		$reqjsonpwd = urlencode(utf8_encode($reqjsonpwd));
		echo "<html><body onload=\"document.forms['fm'].submit();\"><form name=\"fm\" action=\"$url\"
				method=\"post\" ><input type=\"hidden\" name=\"reqjsonpwd\"
				value=\"$reqjsonpwd\"></form></html>";
	}
	
	public function receipt_page( $order_id ){
		echo '<p>' . __('感謝您的訂購，接下來將導向到付款頁面，請稍後！', 'woocommerce') . '</p>';
		
		echo $this->generate_credit_form( $order_id );
		
	}
	
	public function thankyou_page( $order_id )
	{
		global $woocommerce;
		
		$order = new WC_Order( $order_id );
		
		if( isset( $_GET['mccargs'] ) ){
			$mccargs = $_GET['mccargs'];
			$mccargs = str_replace( ' ', '+', $mccargs );
			$mccargs = base64_decode( $mccargs );
			parse_str( $mccargs, $resArr );
			if( $resArr && is_array( $resArr ) ){
				#if ( $description = $this->get_description() )
				#GC_minos:2023-04-21會在感謝頁看起來像多餘的文字echo wpautop( wptexturize( $description ) );
				if ($resArr['StatusCode'] == 'I0000' ) {
					$result_msg = '<p class="payment_result success">信用卡交易成功！</p>';
				}else{
					$result_msg = '<p class="payment_result fail">信用卡交易失敗！<br>失敗原因：' . $resArr['StatusDesc'] . '</p>';
				}
			}else{
				$result_msg = '<p class="payment_result fail">無效的交易參數！</p>';
			}
		}else{
			$result_msg = '<p class="payment_result fail">沒有有效的交易參數！</p>';
		}

		$woocommerce->cart->empty_cart();
		echo $result_msg;
	}

	
	/************************************
	 * 回傳訂單設定直至訂單列表的該訂單 *
	 ************************************/
	 
	public function process_payment( $order_id ) {
		
		global $woocommerce;

		$order = new WC_Order( $order_id );
		
		if( isset( $_POST['ctcb_ceom_credit'] ) ):
			//gc_log( $_POST, "process_payment" );
			update_post_meta( $order_id, 'gc_ctcb_ceom_credit', sanitize_text_field( $_POST['ctcb_ceom_credit'] ) );
		endif;

		return array(
			'result' => 'success',
            'redirect' => add_query_arg(
                'order',
                $order_id,
                add_query_arg(
                    'key',
                    $order->get_order_key(),
                    get_permalink( woocommerce_get_page_id( 'pay' ) )
                )
            )
		);		
	}
	
	public function payment_fields()
	{
		$cards = get_metadata( 'user', get_current_user_id(), 'serialize_cards' );
		
		usort( $cards[0], [ $this, 'card_sort' ] );
		
		//gc_log( $cards[0], 'card_sort' );
		
		if ( $this->description ):
			echo wpautop( wptexturize( $this->description ) );
		endif;
		
		echo '<label for="ctcb_ceom_credit">信用卡選擇</label><select name="ctcb_ceom_credit" id="ctcb_ceom_credit">';		
		foreach( $cards[0] as $card ):
		
				echo '<option value="' . $card['CardToken'] . '">卡號：' . $card['CardNoMask'] . '('.$card['AliasName'].')</option>';
		endforeach;
		echo '</select>';
	}
	
	private static function card_sort( $i, $j )
	{
		return ($i["Default"] == true) ? -1 : 1;
	}
	
	public function disable_payment_gateway( $available_gateways )
	{
		$cards = get_metadata( 'user', get_current_user_id(), 'serialize_cards' );

		if( empty( $cards ) ):
			unset( $available_gateways['gc_ctcb_ceom'] );
		endif;
		
		return $available_gateways;
	}
	
}