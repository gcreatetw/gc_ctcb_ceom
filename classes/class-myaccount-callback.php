<?php
require_once ( '../../../../wp-load.php' );

$payment = new WC_CTCB_COEM_Payment_Gateway();

$current_user = wp_get_current_user();

$uid = $current_user->ID;

$mackey = $payment->settings['key'];

$fastpay = new MacUtil;

$resData = urldecode(utf8_encode($_POST['rspjsonpwd']));

$res=json_decode($resData,true);

$sechead = $res["Response"]["Header"]["ServiceName"];

$sectxn  = $res["Response"]["Data"]["TXN"];

$textData 	= $fastpay->getDecTXN($sectxn,$mackey);

$result = base64_decode($textData);

gc_log( $result, "credit_card" );
gc_log( $sechead, "credit_card" );

if ( $result ){
    parse_str( $result, $resArr );

    if( $resArr['StatusCode'] == 'I0000' ) {

        if ($sechead == "TokenAdd") {

            $duplicated = false;
            $default = false;

            $cards = get_user_meta($resArr['MemberID'], 'serialize_cards', true);
            $default_aliasname = '信用卡' . ( count( $cards ) + 1 );

            if ($cards && is_array($cards)){
                foreach ($cards as $card) {
                    if ($resArr['CardToken'] == $card['CardToken']) {
                        //只要重複任何一張卡就不新增該卡片至user_meta中
                        $duplicated = true;
                        break;
                    }
                }
                //如果卡片不重複就新增到user_meta中
                if (!$duplicated) {
                    $cardData = [
                        "Default" => $default,
                        "AliasName" => $resArr["AliasName"] ?: $default_aliasname,
                        "CardNoMask" => $resArr["CardNoMask"],
                        "CardToken" => $resArr["CardToken"],
                        "RequestNo" => $resArr["RequestNo"],
                        "CardType" => $resArr["CardType"],
                        "BankDesc" => $resArr["BankDesc"],
                    ];
                    array_push($cards, $cardData);
                    update_user_meta($resArr['MemberID'], "serialize_cards", $cards);
                }

            }else{

                //如果使用者目前沒有存任何卡片的話
                $default = true;
                $cardData = [
                    "Default" => $default,
                    "AliasName" => $resArr["AliasName"] ?: $default_aliasname,
                    "CardNoMask" => $resArr["CardNoMask"],
                    "CardToken" => $resArr["CardToken"],
                    "RequestNo" => $resArr["RequestNo"],
                    "CardType" => $resArr["CardType"],
                    "BankDesc" => $resArr["BankDesc"],
                ];
                $cards = [];
                $cards[] = $cardData;
                update_user_meta($resArr['MemberID'], "serialize_cards", $cards);
            }

        }elseif ($sechead == "TokenDelete") {
            gc_log($resArr, "TokenDelete");
            $cards = get_user_meta($resArr['MemberID'], 'serialize_cards', true);
            $nodefault = false;

            if ($cards && is_array($cards)) {

                foreach ($cards as $k => $card) {

                    if ($resArr['RequestNo'] == $card['RequestNo']) {
                        unset($cards[$k]);
                    }

                    if ($card['Default'] == true) {
                        $nodefault = true;
                    }

                }

                if (!empty($cards)) {
                    $cards = array_values($cards);
                    if ($nodefault == true) {
                        $cards[0]['Default'] = true;
                    }
                    update_user_meta($resArr['MemberID'], "serialize_cards", $cards);
                }else {
                    delete_metadata('user', $resArr['MemberID'], "serialize_cards");
                }
            }else {
                delete_metadata('user', $resArr['MemberID'], "serialize_cards");
            }
        }
        wp_redirect(wc_get_account_endpoint_url('ctcb-credit'));
    }else {
        echo "<form name='ctcb' action='" . esc_url(wc_get_account_endpoint_url('ctcb-credit')) . "' method='POST'><input name='callback' type='hidden' value='" . base64_encode(http_build_query($resArr)) . "'></form><script>window.onload=function(){document.forms['ctcb'].submit();}</script>";
    }
}else{
    wp_redirect(site_url());
}
?>