<?php
if (!defined('ABSPATH')) {
    exit;
}

class CTCB_MYACCOUNT_SETTINGS{
	
	private static $instance = null;
	//private $options;
	
    public static function forge()
	{
        if( is_null( self::$instance ) ){
			
            self::$instance = new self;
        }
        return self::$instance;
    }
	
	public function __construct()
	{
		add_action( 'init', [ $this, "custom_endpoint" ], 10 );
		
		add_filter ( 'woocommerce_account_menu_items', [ $this, 'bind_credit' ], 99, 1 );

		add_action( 'woocommerce_account_ctcb-credit_endpoint', [ $this, "action_woocommerce_account_key_endpoint" ] );
		
		add_action( 'wp_enqueue_scripts', [ $this, 'datatables_enqueue_script' ], 10000 );
		
		add_action( 'wp_ajax_get_card', [ $this, 'get_card_info' ] );  
		add_action( 'wp_ajax_edit_card', [ $this, 'edit_card_info' ] );  
		add_action( 'wp_ajax_delete_card', [ $this, 'delete_card_info' ] );  
		
		# GC_Chandler 2022-12-13 取代原版datatable樣式
		add_action('view_edit_creditcard',[$this , 'generate_view_edit_creditcard']);
	}

    /**
     * @Created on 2023-02-22
     * @Created by GC_minos
     * @Description 用來過濾如果卡片中有兩張以上同被註記Default=1 or true時，只留下第一個
     * */
    public function filter_cards( $cards ){
        if( $cards ){
            $has_default = false;
            foreach( $cards as &$user_card ){
                if( ! $has_default && $user_card['Default'] ){
                    $has_default = true;
                    continue;
                }

                if( $has_default ){
                    $user_card['Default'] = false;
                }
            }
        }
        return $cards;
    }
	
	public function get_card_info()
	{
		$current_user = wp_get_current_user();
		$cards 		  = get_user_meta( $current_user->ID, "serialize_cards", true );
		$cards 		  = array_values($cards);
        $cards        = $this->filter_cards( $cards );
		
		gc_log( $cards, "GET_CARDS");
		wp_send_json( $cards );
	}
	
	public function edit_card_info()
	{
		$current_user = wp_get_current_user();
		$cards 		  = get_user_meta( $current_user->ID, "serialize_cards", true );
		
		if( isset( $_POST['alias'] ) ):
		
			foreach( $cards as $key => $card ):
				
				if( $card['RequestNo'] == $_POST['requestno'] ):
				
					$cards[$key]['AliasName'] = $_POST['alias'];
				endif;
			endforeach;
		elseif( isset( $_POST['default'] ) ):
		
			foreach( $cards as $key => $card ):
				$cards[$key]['Default'] = false;
				if( $card['RequestNo'] == $_POST['requestno'] ):
				
					$cards[$key]['Default'] = $_POST['default'];
				endif;
			endforeach;		
		endif;
		
		update_user_meta( $current_user->ID, "serialize_cards", $cards );
	}
	
	public function delete_card_info()
	{
		$options   = get_option('woocommerce_gc_ctcb_ceom_settings');
		$current_user 	= wp_get_current_user();
		$uid 			= $current_user->ID;
		$now 	   = date_i18n("Y/m/d H:i:s");
		$cardtoken = $_POST['cardtoken'];
		$requestno = $_POST['requestno'];
		$paramMap = [
			"MerID" 	=> $options['mer_id'], // 特店代號
			"MemberID" 	=> $uid, // Unique Identifier
			"RequestNo" => $requestno , 
			"CardToken" => $cardtoken , 
			"TokenURL" 	=> plugins_url( GC_CTCB_DIR ) . '/classes/class-myaccount-callback.php'
		];

		arsort($paramMap);

		$data = '';
		foreach($paramMap as $key => $value):
		
			$data .= "&$key=$value";
		endforeach;
		
		$data = substr($data, 1);
		$MchKey = $options['key']; // 特店管理系統 >> 系統設定(信用卡加密) >> 自行指定一個長度24碼(需剛好24碼)的明碼字串，不是使用加密過後的壓碼
		$fastpay = new MacUtil;
		$txn = $fastpay->getTXN($data,$MchKey);

		$mac = $fastpay->getMAC($data,$MchKey);
			
		$secpMap = array(
			"HexMac" => $mac,
			"SecData" => $txn
		);
		$Req = array(
			"Request" => array(
				"Header" => array(
					"ServiceName" 	=> "TokenDelete",
					"Version" 		=> "1.0",
					"MerchantID" 	=> $options['merchant_id'],
					"RequestTime" 	=> $now
				),
				"Data" => array(
					"MAC" => $mac,
					"TXN" => $txn
				)
			)
		);
		$reqjsonpwd = json_encode($Req,JSON_UNESCAPED_SLASHES);

		$reqjsonpwd = urlencode(utf8_encode($reqjsonpwd));
		wp_send_json($reqjsonpwd);
	}
	
	public function datatables_enqueue_script()
	{
		global $wp_query;
		
		if( isset($wp_query->query['ctcb-credit']) ):
		
			wp_enqueue_style( 'local-css', plugins_url( '/assets/css/dataTables.min.css', dirname( __FILE__ ) ), null, '1.11.5', false );
			wp_enqueue_style( 'dataTables.btn.css', plugins_url( '/assets/css/buttons.dataTables.min.css', dirname( __FILE__ ) ), null, '2.2.2', false );
			wp_enqueue_style( 'sweetalert2.dist.css', plugins_url( '/assets/css/sweetalert2.min.css', dirname( __FILE__ ) ), null, '11.4.8', false );
			wp_enqueue_script( 'local-js', plugins_url( '/assets/js/jquery.dataTables.min.js', dirname( __FILE__ ) ), null, '1.11.5', false );
			// wp_enqueue_script( 'dataTables.ajax.js', plugins_url( '/assets/js/dataTables-ajax.js', dirname( __FILE__ ) ), null, '1.0.0', true );
			wp_enqueue_script( 'dataTables.btn.js', plugins_url( '/assets/js/dataTables.buttons.min.js', dirname( __FILE__ ) ), null, '2.2.2', true );
			wp_enqueue_script( 'sweetalert2.dist.js', plugins_url( '/assets/js/sweetalert2.min.js', dirname( __FILE__ ) ), null, '11.4.8', true );
			wp_enqueue_script( 'fontawesome.js', "https://kit.fontawesome.com/ed50d02bad.js", null, '1.0', true );
			// GC_kevin 加入 css
			wp_enqueue_style( 'bonus.main.css', plugins_url( 'gc_ctcb_ceom/assets/css/bonus-main.css' ), null, '1.0.0', false );
		
			wp_localize_script( 'dataTables.ajax.js', 'ajax_url',
			array( 
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
			));
			
			# GC_Chandler
			
			wp_register_script('jquery.tmpl.min.js', GC_CTCB_ASSETS_JS_URL.'jquery.tmpl.min.js' );
			wp_register_script('view-creditcard-list', GC_CTCB_ASSETS_JS_URL.'creditcard-list.js' );
			$args = [
                'ajaxurl' => admin_url("admin-ajax.php"),
            ];
            wp_localize_script('view-creditcard-list', 'parameter', $args);
			wp_register_style( 'view-creditcard-list' , GC_CTCB_ASSETS_CSS_URL. 'creditcard-list.css' );
			
			
			
		endif;
	}
	
	public function custom_endpoint()
	{
		add_rewrite_endpoint('ctcb-credit', EP_ROOT | EP_PAGES);
	}
	
	public function bind_credit( $items )
	{
		$items['ctcb-credit'] = __('信用卡管理', 'txtdomain');
		
		return $items;
	}
	
	public function action_woocommerce_account_key_endpoint()
	{
		$options 		= get_option('woocommerce_gc_ctcb_ceom_settings');
		$current_user 	= wp_get_current_user();
		$uid 			= $current_user->ID;
		//$cards 			= get_user_meta( $uid, "serialize_cards" );
		$cards 			= get_user_meta( $uid, "serialize_cards", true );
		$now 			= date_i18n("Y/m/d H:i:s");
		$callback 		= plugins_url( GC_CTCB_DIR ) . '/classes/class-myaccount-callback.php';
		$RequestNo 		= time();
		$url 			= $options['gateway'];
		$curl 			= curl_init();
		
		if( $_POST ):
			gc_log( base64_decode($_POST['callback']), "BundleCardError" );
			echo wc_add_notice("綁定信用卡失敗，請洽系統管理員", 'error' );
		endif;
		
		include dirname( __DIR__, 1 ) . '/templates/myaccount/header.php';
		
		/****************
		 * 新增卡片區塊 *
		 ****************/
		$paramMap = array(
			"MerID" 	=> $options['mer_id'], // 特店代號
			"MemberID" 	=> $uid, // Unique Identifier
			"RequestNo" => $RequestNo, 
			"TokenURL" 	=> $callback, //特店的回傳結果參數指定的回傳網址,
			//GC_minos:2025-02-05 以下兩個參數暫時取消傳送，因為會導致新增信用卡失敗
			//"PhoneNum"  => '',
			//"PhoneNumEditable" => ''
		);

		arsort($paramMap);

		$data = '';
		foreach($paramMap as $key => $value):
		
			$data .= "&$key=$value";
		endforeach;
		
		$data = substr($data, 1);
		$MchKey = $options['key']; // 特店管理系統 >> 系統設定(信用卡加密) >> 自行指定一個長度24碼(需剛好24碼)的明碼字串，不是使用加密過後的壓碼
		$fastpay = new MacUtil();
		$txn = $fastpay->getTXN($data,$MchKey);

		$mac = $fastpay->getMAC($data,$MchKey);
			
		$secpMap = array(
			"HexMac" => $mac,
			"SecData" => $txn
		);
	
		$Req = array(
			"Request" => array(
				"Header" => array(
					"ServiceName" 	=> "TokenAdd",
					"Version" 		=> "1.0",
					"MerchantID" 	=> $options['merchant_id'],
					"RequestTime" 	=> $now
				),
				"Data" => array(
					"MAC" => $mac,
					"TXN" => $txn
				)
			)
		);
		$reqjsonpwd = json_encode($Req,JSON_UNESCAPED_SLASHES);

		$reqjsonpwd = urlencode(utf8_encode($reqjsonpwd));
		
		
		include dirname( __DIR__, 1 ) . '/templates/myaccount/bind-card.php';
		# wp_localize_script( 'dataTables.ajax.js', 'url', $url );
		# wp_localize_script( 'dataTables.ajax.js', 'reqjsonpwd', $reqjsonpwd );
		wp_localize_script( 'view-creditcard-list', 'url', $url );
		wp_localize_script( 'view-creditcard-list', 'reqjsonpwd', $reqjsonpwd );
		include dirname( __DIR__, 1 ) . '/templates/myaccount/dataTables.php';
		include dirname( __DIR__, 1 ) . '/templates/myaccount/footer.php';
	}
	
	public function generate_view_edit_creditcard()
	{
		global $current_user;
		
		$user_cards = get_user_meta( $current_user->ID, "serialize_cards", true );
        $user_cards = $this->filter_cards( $user_cards );
		
		require_once GC_CTCB_TEMP_PATH .'views/creditcard-list.php';
	}
}
return new CTCB_MYACCOUNT_SETTINGS();