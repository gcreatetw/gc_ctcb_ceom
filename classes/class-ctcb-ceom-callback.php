<?php
	require_once ( '../../../../wp-load.php' );
	
	$payment = new WC_CTCB_COEM_Payment_Gateway();
		
	$mackey = $payment->settings['key'];
	
	$fastpay = new MacUtil;
	
	$resData = urldecode(utf8_encode($_POST['rspjsonpwd']));
	
	$res=json_decode($resData,true);
	
	$sectxn  = $res["Response"]["Data"]["TXN"];

	$textData 	= $fastpay->getDecTXN($sectxn,$mackey);
	
	$result = base64_decode($textData);
	
	parse_str( $result, $resArr );
	
	$order_id = $resArr['Lidm'];
	$order = new WC_Order( $order_id );
    $order_status = $order->get_status();
    gc_log( $resArr, 'credit_card_my' );
    
	if ( $result && !in_array( $order_status, array( 'processing', 'completed' ) ) ) {
		if( $resArr['StatusCode'] == 'I0000' ) {
			#GC_minos照理說不用，因為之前有問題是因為沒有在這裡就修改訂單狀態$order->update_meta_data( 'RequestNo', $resArr['RequestNo'] ); #GC_minos:2023-09-15將付款完成的時間獨立紀錄並在傳送給ERP時，取用該值
			$order->update_meta_data( 'last4digitpan', $resArr['Last4digitPAN'] ); #GC_minos:2023-08-07
            $order->save();
			#GC_minos:2023-05-06改回processing
			if( gc_is_ticket_order( $order ) ){
				$order->update_status( 'processing', '卡號末四碼：' . $resArr[ 'Last4digitPAN' ] . '，收到付款。' );
			}else{
				$order->update_status( 'processing', '卡號末四碼：' . $resArr[ 'Last4digitPAN' ] . '，收到付款。' );
			}
			// $url = $payment->get_return_url( $order ) . '&amp;args=' . base64_encode(http_build_query($result));
			$url = $payment->get_return_url( $order ) . '&mccargs=' . base64_encode( $result );
			gc_log( '1 url : ' . $url , 'credit_card_my' );
		}else{
			//GC_minos:2023-09-05將錯誤訊息StatusDesc附加在note
			$order->add_order_note('系統回傳交易失敗：' . $resArr['StatusDesc'] );
            //GC_minos:2022-11-17失敗也要導向結果頁，在結果頁加入失敗的訊息
			$url = $order->get_checkout_order_received_url() . '&mccargs=' . base64_encode( $result );
			gc_log( '2 url :' . $url, 'credit_card_my' );
		}
		wp_redirect( $url );
	} else {
		wp_redirect(site_url());
	}

