<?php

defined( 'ABSPATH') or die();

class CTCB_Cron{
    public function __construct(){
        add_action( 'init', [ $this, 'set_cron' ] );

        add_action( 'ctcb_check_pending_orders',  [ $this, 'check_pending_orders_status' ] );
    }

    public function set_cron(){
        if (!wp_next_scheduled('ctcb_check_pending_orders')) {
            wp_schedule_event( time(), 'every_minute', 'ctcb_check_pending_orders' );
        }
    }

    function check_pending_orders_status() {
        include_once( dirname( __DIR__ ) . '/function/POSAPI.php' );
        // 获取所有等付款中(wc-pending)的订单
        $args = array(
            'status' => 'wc-pending',
            'limit' => -1,
        );
        $orders = wc_get_orders($args);
    
        if( ! $orders ){
            //gc_log('沒有符合的訂單');
            return;
        }

        $ctcb_setting = get_option('woocommerce_ctcb_credit_settings', [] );

        if( ! $ctcb_setting ){
            gc_log('沒有ctcb設定');
            return;
        }

        gc_log('有符合的訂單，共' . count($orders) . '筆');

        foreach ($orders as $order) {
            $order_id = $order->get_id();
            $order_number = $order->get_order_number(); // 假设这个是LID-M
    
            // 设置查询参数
            $server = array(
                'URL' => $ctcb_setting['gateway'],
                'Timeout' => 30,
                'MacKey' => $ctcb_setting['key']
            );
            $inquiry = array(
                'TX_ATTRIBUTE' => 'TX_AUTH', // 根据需要修改
                'MERID' => $ctcb_setting['mer_id'],
                'LID-M' => $order_id,
                'PAN' => '', // 信用卡卡號，網站怎麼可能會存啦
                'currency' => '901', // 台幣
                'purchAmt' => $order->get_total(),
                'RECUR_NUM' => 0,
                'PRODCODE' => ''
            );
    
            gc_log( $server );
            gc_log( $inquiry );
            // 调用中国信托 API
            $response = InquiryTransac($server, $inquiry);
            gc_log($response);
    
            // 检查返回状态
            if (isset($response['RespCode']) && $response['RespCode'] == 0) {
                $current_state = $response['CurrentState'];
                if ($current_state == 1) { // 授权成功
                    // 更新订单状态为处理中(wc-processing)
                    $order->update_status('wc-processing');
                }
            }
        }
    }    
}

new CTCB_Cron();