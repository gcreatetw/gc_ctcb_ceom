jQuery(document).ready(function($) {
    
	get_card_data();
	
	var creditcard_row = $('.creditcard-row');
    // # 編輯
    creditcard_row.on('click', 'a[name=edit-creditcard]', function(e) {
		let cardinfo = $(this).data('cardinfo');
		Swal.fire({
			title: '更新卡片別名',
			input: 'text',
			inputAttributes: {
				autocapitalize: 'off'
			},
			showCancelButton: true,
			confirmButtonText: '更新',
			cancelButtonText: '取消',
			showLoaderOnConfirm: true,
			preConfirm: (alias) => {}
		}).then((result) => {
			if (result.isConfirmed == true) {
				$('.relation-bg').css('z-index', '99');
				$.ajax({
					url:parameter.ajaxurl,
					type:'POST',
					data:{
						"action":"edit_card",
						"alias":result.value,
						"requestno":cardinfo.RequestNo
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						get_card_data();
					}
				});
			}
		});
    });
	
	// # 預設
    creditcard_row.on('click', 'a[name=default-creditcard]', function(e) {
		
		console.log('click default-creditcard');

		let cardinfo = $(this).data('cardinfo');
		
		Swal.fire({
			  title: '設定為預設卡片?',
			  showCancelButton: true,
			  confirmButtonText: '儲存',
			  cancelButtonText: '取消',
		}).then((result) => {
			
			if (result.isConfirmed) {
				$('.relation-bg').css('z-index', '99');
				$.ajax({
					url:parameter.ajaxurl,
					type:'POST',
					data:{
						"action":"edit_card",
						"default":true,
						"requestno":cardinfo.RequestNo
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						get_card_data();
					}
				});
			 }
		});
		
    });
	
	// # 刪除
	creditcard_row.on('click', 'a[name=delete-creditcard]', function(e) {
		Swal.fire({
			title: '確認刪除卡片?',
			text: "刪除後需另外新增卡片!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#9c2222',
			cancelButtonColor: '#6e7881',
			confirmButtonText: '確認, 刪除卡片!',
			cancelButtonText: '取消'
		}).then((result) => {
			if (result.isConfirmed) {
				let cardinfo = $(this).data('cardinfo');
				
				$.ajax({
					url:parameter.ajaxurl,
					type:'POST',
					data:{
						"action":"delete_card",
						"requestno":cardinfo.RequestNo,
						"cardtoken":cardinfo.CardToken
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						console.log(url);
						$('<form action="'+url+'" method="POST" ><input type="hidden" name="reqjsonpwd" value="'+datas+'"></form>').appendTo('body').submit();						
						get_card_data();
					}
				});
			}
		});
	});
	
	function get_card_data()
	{
		$.ajax({
			url:parameter.ajaxurl,
			type:'GET',
			data:{"action":"get_card"},
			dataType:'json',
			success: function(data, textStatus, request){
				if( data ) {
					creditcard_list(data);
				}
				$('.relation-bg').css('z-index', '-1');
			},
			error: function (request, textStatus, errorThrown) {
				alert(request.getResponseHeader('some_header'));
			}
		});
	}
	
	function creditcard_list(cardData)
	{
		let creditcard_list_html = column = '';
		creditcard_row.empty();
		$.each( cardData, function( key, fields ) {
			column += '<div class="column swiper-slide">';
			fields['Default'] ?  column += '<div class="box activity">' : column += '<div class="box ">';
			column += `<a href="#" name="delete-creditcard" class="close" data-cardinfo='${JSON.stringify(fields)}'><i class="fa-solid fa-xmark"></i></a>`;
			column += '<i class="fa-brands fa-cc-'+ fields['CardType'].toLowerCase() +' creditcard-icon"></i>';
			column += '<h4 class="btn-alias">'+fields['AliasName'] +'</h4>';
			column += '<div class="info">' + fields['CardNoMask'] + '</div>';
			column += '<div class="link">';
			fields['Default'] ? column += `<a href="#"><i class="fa-solid fa-check"></i>預設</a>` : column += `<a href="#" name="default-creditcard" data-cardinfo='${JSON.stringify(fields)}'>設成預設</a>`;
			column += `<a href="#" name="edit-creditcard" data-cardinfo='${JSON.stringify(fields)}' >編輯</a>`;
			column += '</div></div></div>';
		});
		creditcard_list_html = column;
		creditcard_row.append(creditcard_list_html);
		swiper = new Swiper('.swiper',{
			slidesPerView:'1.2',
			breakpoints: {
				768: {
					slidesPerView: '3'
				}
			}
		});
	}
	
	function show_saving_loading(){
		
		var content = '<div class="loadingio-spinner-message-wngslu5543"><div class="ldio-m0uwhi477co">' +
				'<div></div><div></div><div></div>' +
				'</div></div>';
		$.blockUI({
			blockMsgClass: 'loading-beans',
			message: content,
			css: {
				center: true,
				width: '50%',
				top: '40%',
				left: '25%',
				backgroundColor: '#ffffff00',
				border: '0px solid'
			}
		});
	}
	
	function countdown_timer( second ){
		var i = second;
		var timer = setInterval(function(){			
			console.log('second',i);		
			if(i == 0) {
				$.unblockUI();
				clearInterval(timer);
			}
			i--;
		},1000);
	};
});