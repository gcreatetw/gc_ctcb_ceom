jQuery( document ).ready(function($) {
	get_card_data();
	function get_card_data()
	{
		$.ajax({
			url:ajax_url.ajaxurl,
			type:'GET',
			data:{"action":"get_card"},
			dataType:'json',
			success: function(data, textStatus, request){
				if( data ) {
					datatables(data);
				}
				$('.relation-bg').css('z-index', '-1');
			},
			error: function (request, textStatus, errorThrown) {
				alert(request.getResponseHeader('some_header'));
			}
		});
	}
	function datatables( cardData )
	{
		$.extend( $.fn.dataTable.defaults, {
			searching: true,
			ordering:  true
		} );
		$('#credit-list').DataTable({
			destroy: true,
			responsive: true,
			searching: false,
			paging: true,
			data:cardData,
			dom: "Bfrt",
			columns: [{
					title:"卡號",
					data:"CardNoMask"
				},{
					title: "發卡機構",
					data: "CardType"
				},{
					title: "發卡銀行描述",
					data: "BankDesc"
				},
				{
					title:"卡片別名",
					data:"AliasName"
				},{
					title: "預設卡片",
					data: "Default"
				},{
					data: 'CardToken',
					sortable: false,
					"render": function ( data, type, full, meta ) {
						return '<i class="fa-solid fa-circle-trash"></i> 刪除';
					}
				},
			],
			buttons: [
				{
					text: "新增卡片",
					action: function (e, dt, node, config) {
						if (cardData === null) {
							$('<form action="' + url + '" method="POST" ><input type="hidden" name="reqjsonpwd" value="' + reqjsonpwd + '"></form>').appendTo('body').submit();
						} else {
							if (cardData.length <= 5) {
								$('<form action="' + url + '" method="POST" ><input type="hidden" name="reqjsonpwd" value="' + reqjsonpwd + '"></form>').appendTo('body').submit();
							} else {
								alert("綁卡已達五張上限，請移除其他卡片後再新增");
							}
						}
					}
				}
				],
			columnDefs: [
				{ className: "dt-center btn-alias", targets: [ 3 ] },
				{ className: "dt-center btn-default", targets: [ 4 ] },
				{ className: "dt-center btn-alias", targets: [ 1 ] },
				{ className: "dt-center btn-default", targets: [ 2 ] },
				{ className: "dt-center btn-delete", targets: [ -1 ] },
			],
			language: {
				zeroRecords: "沒有資料顯示，尚未綁定卡片"
			}
		});
		
		var table = $('#credit-list').DataTable();
		
		table.on( 'click', 'td.btn-alias', function () {
			var data = table.row( $(this).parents('tr') ).data();
		
			Swal.fire({
			  title: '更新卡片別名',
			  input: 'text',
			  inputAttributes: {
				autocapitalize: 'off'
			  },
			  showCancelButton: true,
			  confirmButtonText: '更新',
			  cancelButtonText: '取消',
			  showLoaderOnConfirm: true,
			  preConfirm: (alias) => {}
			}).then((result) => {
			  if (result.isConfirmed == true) {
				$('.relation-bg').css('z-index', '99');
				$.ajax({
					url:ajax_url.ajaxurl,
					type:'POST',
					data:{
						"action":"edit_card",
						"alias":result.value,
						"requestno":data.RequestNo
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						get_card_data();
					}
				});
			  }
			})
		});
		
		table.on( 'click', 'td.btn-default', function () {
			var data = table.row( $(this).parents('tr') ).data();
		
			Swal.fire({
			  title: '設定為預設卡片?',
			  showCancelButton: true,
			  confirmButtonText: '儲存',
			  cancelButtonText: '取消',
			}).then((result) => {
			  if (result.isConfirmed) {
				$('.relation-bg').css('z-index', '99');
				$.ajax({
					url:ajax_url.ajaxurl,
					type:'POST',
					data:{
						"action":"edit_card",
						"default":true,
						"requestno":data.RequestNo
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						get_card_data();
					}
				});
			  }
			})
		});
		
		table.on( 'click', 'td.btn-delete', function () {
			Swal.fire({
			  title: '確認刪除卡片?',
			  text: "刪除後需另外新增卡片!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: '確認, 刪除卡片!',
			  cancelButtonText: '取消'
			}).then((result) => {
			  if (result.isConfirmed) {
				
				var data = table.row( $(this).parents('tr') ).data();
				$.ajax({
					url:ajax_url.ajaxurl,
					type:'POST',
					data:{
						"action":"delete_card",
						"requestno":data.RequestNo,
						"cardtoken":data.CardToken
					},
					dataType:'json',
					success: function(datas, textStatus, request){
						$('<form action="'+url+'" method="POST" ><input type="hidden" name="reqjsonpwd" value="'+datas+'"></form>').appendTo('body').submit();						
						get_card_data();
					}
				});
			  }
			})
		});
	}
});
